# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup 
    @new_stack = Stack.new
  end

  def teardown; end

  def test_should_convert_stack_to_array
    assert(@new_stack.to_a == [])
  end

  def test_should_push_element_to_stack
    @new_stack.push!('element')
    assert(@new_stack.to_a == ['element'])
  end

  def test_should_pop_element_from_stack
    @new_stack.push!('ruby')
    @new_stack.push!('rails')
    @new_stack.pop!
    assert(@new_stack.to_a == ['ruby'])
  end

  def test_should_check_if_is_empty
    assert(@new_stack.to_a.empty?)
  end

  def test_should_check_if_is_not_empty
    @new_stack.push!('ruby')
    assert(@new_stack.to_a.empty? == false)
  end

  def test_should_be_not_zero_size
    (1..3).each { |el| @new_stack.push!(el) }
    assert(@new_stack.size == 3)
  end

  def test_should_clear_after_push
    @new_stack.push!('value')
    @new_stack.clear!
    assert(@new_stack.empty? )
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
