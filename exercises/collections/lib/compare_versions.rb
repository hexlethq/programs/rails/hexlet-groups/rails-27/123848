# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  v1_numbers = version1.split('.')
  v2_numbers = version2.split('.')

  v1_numbers.map!(&:to_i)
  v2_numbers.map!(&:to_i)

  v1_numbers <=> v2_numbers
end
# END
