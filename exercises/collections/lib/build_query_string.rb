# frozen_string_literal: true

# BEGIN

def build_query_string(params)
  array = []
  params.each_pair { |key, value| array.push([key, value]) }
  array.sort!

  array.map! { |item| "#{item[0]}=#{item[1]}" }
  array.join('&')
end

# END
