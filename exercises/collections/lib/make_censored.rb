# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  tokens = text.split
  stop_words_hash = {}

  stop_words.each do |word|
    stop_words_hash[word] = true
  end

  tokens.map! { |token| stop_words_hash.key?(token) ? '$#%!' : token }

  tokens.join(' ')
  # END
end
