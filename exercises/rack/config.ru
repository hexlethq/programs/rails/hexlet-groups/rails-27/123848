require_relative 'lib/app'

Rack::Handler::Thin.run App.new, Port: 3000, Host: '0.0.0.0'
