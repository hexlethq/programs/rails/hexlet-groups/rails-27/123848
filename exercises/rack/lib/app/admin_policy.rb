#*lib/app/admin_policy.rb* - возвращает *403* статус ответа для всех запросов, 
#путь которых начинается с */admin*. Возвращать тело ответа при этом не нужно

class AdminPolicy
  def initialize(app)
    @app = app
  end

  def call(env)
    puts env, ' admin policy'
    status, headers, body = @app.call(env)
  end
end