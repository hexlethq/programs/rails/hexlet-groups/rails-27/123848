# frozen_string_literal: true

require 'rack'
require 'thin'
require_relative 'app/admin_policy'
require_relative 'app/execution_timer'
require_relative 'app/signature'
require_relative 'app/router'

class App
  def initialize
    Rack::Builder.new do |builder|
      builder.use AdminPolicy
      # # BEGIN
      # # END
      # builder.use Signature
      # builder.run Router.new
    end
  end

  def call(env)
    [200, {"Content-Type" => "text/html"}, "<h1>Heaall yeah</h1>"]
  end
end
