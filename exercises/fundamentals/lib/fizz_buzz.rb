# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  numbers = (start..stop).to_a
  return nil if numbers.empty?

  result = []

  numbers.each do |number|
    buzz = (number % 5).zero?
    fizz = (number % 3).zero?
    fizz_buz = buzz && fizz

    if fizz_buz
      result.push('FizzBuzz')
    elsif buzz
      result.push('Buzz')
    elsif fizz
      result.push('Fizz')
    else
      result.push(number.to_s)
    end
  end

  result.join(' ')
end
# END
