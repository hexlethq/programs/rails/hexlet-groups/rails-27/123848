# frozen_string_literal: true

# BEGIN
def reverse(string)
  array = string.chars
  result = []

  index = array.length - 1
  while index >= 0
    result.push(array[index])
    index -= 1
  end

  result.join
end

string = reverse('Hexlet')
puts string
# END
