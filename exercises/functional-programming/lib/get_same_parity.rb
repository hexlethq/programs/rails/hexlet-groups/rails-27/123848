# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  return list if list.empty?
  
  parity = list[0].even?
  list.filter { |item| item.even? == parity }
end
# END
