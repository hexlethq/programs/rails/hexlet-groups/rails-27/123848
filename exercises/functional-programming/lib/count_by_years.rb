# frozen_string_literal: true

# BEGIN
def count_by_years(entries)
  filtered_by_gender = entries.filter { |entry| entry[:gender] == 'male' }
  birthdays = {}

  filtered_by_gender.each do |item|
    current_birthday = item[:birthday].split('-')[0]

    if birthdays.key? current_birthday
      birthdays[current_birthday] += 1
    else
      birthdays[current_birthday] = 1
    end
  end

  birthdays
end
# END
