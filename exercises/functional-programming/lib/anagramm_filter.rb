# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, words_to_check)
  sorted_word = word.chars.sort
  words_to_check.filter { |item| item.chars.sort == sorted_word }
end
# END
