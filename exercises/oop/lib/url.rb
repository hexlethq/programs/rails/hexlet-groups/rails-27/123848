# frozen_string_literal: true

require 'forwardable'
require 'uri'

# BEGIN
class Url
  extend Forwardable
  include Comparable

  def initialize(url)
    @uri = URI(url)
    @params = {}

    return if @uri.query.nil?
  
    tokens = @uri.query.split('&')
    tokens.each do |item|
      key, value = item.split('=')
      @params[key.to_sym] = value
    end
  end

  def query_params
    @params
  end

  def query_param(name, default = nil)
    @params.fetch(name, default)
  end

  def <=>(other)
    this = [scheme, host, port]
    pairs = query_params.to_a.sort
    this.concat(pairs)

    that = [other.scheme, other.host, other.port]
    other_pairs = other.query_params.to_a.sort
    that.concat(other_pairs)

    this <=> that
  end

  def_delegator :@uri, :scheme, :scheme
  def_delegator :@uri, :host, :host
  def_delegator :@uri, :port, :port
end

# END
